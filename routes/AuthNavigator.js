import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Dashboard from "../screens/Dashboard";
import HireMechanic from "../screens/HireMechanic";
import FindMechanic from "../screens/FindMechanic";
import MechanicWay from "../screens/MechanicWay";
import Settings from "../screens/Settings";
import ChangePassword from "../screens/ChangePassword";
import InstantChat from "../screens/InstantChat";
import BookingList from "../screens/BookingList";
import SingleBooking from "../screens/SingleBooking";
import DashBoardAdmin from "../screens/DashBoardAdmin";
import Bookings from "../screens/Bookings";
import NewBooking from "../screens/NewBooking";
import FindMechanic2 from "../screens/FindMechanic2";

const Stack = createStackNavigator();

function AuthNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
   
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="HireMechanic" component={HireMechanic} />
        <Stack.Screen name="FindMechanic" component={FindMechanic} />
        <Stack.Screen name="MechanicWay" component={MechanicWay} />
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="InstantChat" component={InstantChat} />
        <Stack.Screen name="BookingList" component={BookingList} />
        <Stack.Screen name="SingleBooking" component={SingleBooking} />
        <Stack.Screen name="Bookings" component={Bookings} />
        <Stack.Screen name="NewBooking" component={NewBooking} />
        <Stack.Screen name="FindMechanic2" component={FindMechanic2} />
        <Stack.Screen name="DashBoardAdmin" component={DashBoardAdmin} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AuthNavigator;
