import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import ForgotPassword from '../screens/ForgotPassword';
import RatingForMechenicRider from '../screens/RatingForMechenicRider';


const Stack = createStackNavigator();

function NoAuthNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none">
            <Stack.Screen name="RatingForMechenicRider" component={RatingForMechenicRider} />
                <Stack.Screen name="SignIn" component={SignIn} />
                <Stack.Screen name="SignUp" component={SignUp} />
                <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default NoAuthNavigator;