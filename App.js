import React from "react";
import { StyleSheet, Text, View } from "react-native";
import AuthNavigator from "./routes/AuthNavigator";
import NoAuthNavigator from "./routes/NoAuthNavigator";


export default function App() {
  return <AuthNavigator />
}
