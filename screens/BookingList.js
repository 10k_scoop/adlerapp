import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import BookingViewcard from "../components/BookingViewcard";
import { FontAwesome } from '@expo/vector-icons';

export default function BookingList({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}

            <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.Title}>
                        <Text style={{ fontSize: rf(18), fontWeight: "700" }}>Bookings</Text>
                    </View>

                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Majid Ali" num="Ref# 1245" onPress={()=>navigation.navigate('SingleBooking')} />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Zeeshan khaskheli" num="Ref# 4585" onPress={()=>navigation.navigate('Bookings')}/>
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Noman khaskheli" num="Ref# 5852" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Muzamil ahmed" num="Ref# 0215" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Farhan ali" num="Ref# 1010" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Majid Ali" num="Ref# 1245" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Zeeshan khaskheli" num="Ref# 4585" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Noman khaskheli" num="Ref# 5852" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Muzamil ahmed" num="Ref# 0215" />
                    <BookingViewcard img={require('../assets/categoryIcons/profile2.jpg')} name="Farhan ali" num="Ref# 1010" />

                </ScrollView>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        backgroundColor: "#e5e5e5",
        alignItems: "center",
    },
    Title: {
        width: wp('90%'),
        height: hp('10%'),
        marginTop: 10,
        justifyContent: "center",
        paddingHorizontal: 10
    }

});
