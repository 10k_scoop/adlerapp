import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import Button1 from "../components/Button1";
import { Entypo } from '@expo/vector-icons';

export default function MechanicWay({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}
            <View style={styles.main}>
                <View style={styles.RiderBar}>
                    <Image
                        source={require('../assets/categoryIcons/Rider.png')}
                        resizeMode="contain"
                        style={{ width: "30%", height: "80%" }}
                    />
                    <Text style={{ fontSize: rf(16), fontWeight: "500" }}>Mechanic on its Way</Text>
                </View>

                <View style={styles.MechanicData}>
                    <View style={styles.Inner1}>
                        <View style={styles.Profile}>
                            <Image
                                source={require('../assets/categoryIcons/profile.png')}
                                resizeMode="contain"
                                style={{ width: "100%", height: "100%" }}
                            />
                        </View>
                        <View style={styles.InnerData}>
                            <View style={styles.Name}>
                                <Text style={{ fontSize: rf(16), fontWeight: "700" }}>khadim Hussain </Text>
                                <Text style={{ fontSize: rf(14), color: "grey" }}>Ref no #4534 </Text>
                            </View>
                            <View style={styles.Stars}>
                                <View style={styles.innerStar}>
                                    <Entypo name="star" size={rf(14)} color="yellow" />
                                    <Entypo name="star" size={rf(14)} color="yellow" />
                                    <Entypo name="star" size={rf(14)} color="yellow" />
                                    <Entypo name="star" size={rf(14)} color="grey" />
                                    <Entypo name="star" size={rf(14)} color="grey" />
                                </View>
                                <Text style={{ fontSize: rf(14), color: "grey", fontWeight: "700" }}>(Mechanic)</Text>
                            </View>

                        </View>
                    </View>
                </View>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        backgroundColor: "#e5e5e5"
    },
    MechanicData: {
        width: wp('100%'),
        height: hp('30%'),
        backgroundColor: "#fff",
        position: "absolute",
        bottom: 0
    },
    Inner1: {
        width: "100%",
        height: "40%",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10
    },
    Profile: {
        width: hp('8%'),
        height: hp('9%'),
        borderRadius: 100,
    },
    Name: {
        width: "95%",
        height: "50%",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        alignItems: "flex-end",
        paddingVertical: 4
    },
    InnerData: {
        width: "90%",
        height: "90%",
    },
    Stars: {
        width: "95%",
        height: "50%",
        flexDirection: "row",
        paddingHorizontal: 10,

    },
    innerStar: {
        width: "75%",
        height: "100%",
        flexDirection: "row"
    },
    RiderBar: {
        width: wp('90%'),
        height: hp('7%'),
        backgroundColor: "#fff",
        borderRadius: 10,
        position: "absolute",
        top: "50%",
        right: 20,
        flexDirection: "row",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    }



});
