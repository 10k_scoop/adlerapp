import React from 'react';
import { ImageBackground, StyleSheet, Text, View, Image,TouchableOpacity } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default function ChooseOption() {
    return (

        <View style={styles.container}>
            <ImageBackground style={styles.ImageContainer} source={require('../assets/Group.png')} resizeMode='contain'>
                <View style={styles.TextContainer}>
                    <Text style={{ fontSize: rf(22), color: '#fff' }}>Select</Text>
                </View>
                <View style={styles.TextContainer}>
                    <Text style={{ fontSize: rf(22), color: '#fff', marginLeft: wp('10%') }}>Role</Text>
                </View>
                <View style={styles.QRContainer}>
                    <View style={styles.QRBody}>
                        <Image style={{ height: '100%', width: '60%' }} source={require('../assets/Qrcode.png')} />
                    </View>
                </View>
            </ImageBackground>
            <View style={styles.SelectorContainer}>
                <TouchableOpacity style={styles.SelectorBody}>
                    <Image style={{height:'70%',width:'70%'}}source={require('../assets/Home.png')} 
                    resizeMode='center'/>
                </TouchableOpacity>
                <View style={styles.Icons}></View>
                <View style={styles.Icons}></View>
                <View style={styles.Icons}></View>
                <TouchableOpacity style={styles.SelectorBody}>
                    <Image style={{height:'70%',width:'70%'}}source={require('../assets/Rider.png')} 
                    resizeMode='center'/>
                </TouchableOpacity>
                
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C4E2A6',


    },
    ImageContainer: {
        height: hp('65%'),
        width: wp('100%'),
        backgroundColor: '#C4E2A6',
        justifyContent: 'flex-end'
    },


    QRContainer: {
        height: '30%',
        width: '100%',
        alignItems: "center",
        bottom: 20
    },
    TextContainer: {
        paddingHorizontal: wp('5%')
    },
    QRBody: {
        height: '100%',
        width: '60%',
        alignItems: 'flex-end'

    },
    SelectorContainer: {
        height: hp('18%'),
        width: wp('100%'),
        paddingHorizontal: wp('5%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between'
    },
    SelectorBody: {
        height: hp('15%'),
        width: hp('15%'),
        backgroundColor: '#B3D491',
        borderRadius: 100,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,

        elevation: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Icons:{
        height:hp('2%'),
        width:hp('2%'),
        backgroundColor: '#B3D491',
        borderRadius:100
    }


});
