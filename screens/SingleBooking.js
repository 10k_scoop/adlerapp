import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from 'expo-linear-gradient';
import Header1 from "../components/Header1";
import BookingViewcard from "../components/BookingViewcard";
import { FontAwesome } from '@expo/vector-icons';

export default function SingleBooking({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}

            <View style={styles.main}>
                <View style={styles.Title}>
                    <Text style={{ fontSize: rf(18), fontWeight: "700" }}>Bookings Details</Text>
                </View>
                <View style={styles.Card}>
                    <LinearGradient
                        // Background Linear Gradient
                        colors={['#8FBC80', '#B3D491']}
                        start={[0.0, 0.5]} end={[1.0, 0.5]} locations={[0.0, 1.0]}
                        style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            height: "100%",
                            borderRadius: 15

                        }}
                    />
                    <View style={styles.FirstRow}>
                        <View style={styles.profile}>
                            <Image
                                source={require('../assets/categoryIcons/profile2.jpg')}
                                style={{ width: "100%", height: "100%" }}
                            />
                        </View>
                        <View style={styles.name}>
                            <Text style={{ fontSize: rf(16), color: "#fff" }}>Majid Ali</Text>
                        </View>
                        <View style={styles.Reference}>
                            <Text style={{ fontSize: rf(14), color: "#fff" }}>Ref# 2554</Text>
                        </View>
                    </View>
                    <View style={styles.SecondRow}>
                        <Text style={{ fontSize: rf(16), color: "#fff" }}>Service : plumber</Text>
                        <Text style={{ fontSize: rf(16), color: "#fff" }}>Area : Qasimabad</Text>
                        <Text style={{ fontSize: rf(16), color: "#fff" }}>Contact : 03052487430</Text>
                    </View>

                    <View style={styles.ThirdRow}>
                        <Text style={{fontSize:rf(24),color:"#fff",fontWeight:"700"}}>Status</Text>
                        <View style={styles.status}>
                        <Text style={{fontSize:rf(18),color:"#222",fontWeight:"700"}}>on its way</Text>
                        </View>
                        <Text style={{fontSize:rf(22),color:"#fff",fontWeight:"700"}}>Estimated time</Text>
                        <View style={styles.status}>
                        <Text style={{fontSize:rf(18),color:"#222",fontWeight:"700"}}>25 min</Text>
                        </View>
                    </View>

                </View>

            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        backgroundColor: "#e5e5e5",
        alignItems: "center",

    },
    Title: {
        width: wp('90%'),
        height: hp('10%'),
        marginTop: 10,
        justifyContent: "center",
        paddingHorizontal: 10
    },
    Card: {
        width: wp('90%'),
        height: hp('50%'),
        borderRadius: 15,
        overflow: 'hidden',
        paddingHorizontal: 5
    },
    FirstRow: {
        width: "100%",
        height: "15%",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,

    },
    profile: {
        width: hp('6%'),
        height: hp('6%'),
        borderRadius: 100,
        overflow: "hidden"
    },
    name: {
        width: "45%",
        height: "50%",
        paddingHorizontal: 10,
        justifyContent: "center"
    },
    Reference: {
        width: "40%",
        height: "50%",
        paddingHorizontal: 5,
        alignItems: "flex-end",
        justifyContent: "center"
    },
    SecondRow: {
        width: "100%",
        height: "30%",
        justifyContent: "center",
        paddingHorizontal: 10
    },
    ThirdRow: {
        width: "100%",
        height: "45%",
        alignItems: "center",
        justifyContent:"space-around"
    },
    status:{
        width:"27%",
        height:"20%",
        backgroundColor:"#fff",
        borderRadius:100,
        justifyContent:"center",
        alignItems:"center"
    }

});
