import React from "react";
import { StyleSheet, Text, View, Image,TouchableOpacity } from "react-native";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import CurveyBgDesign from "../components/CurveyBgDesign";
import TextField from "../components/TextField";
import Button1 from "../components/Button1";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
export default function SignIn({navigation}) {
  return (
    <View style={styles.body}>
      {/* backgroundColor Layer */}
      <View style={styles.bgLayer}></View>
      <View style={styles.container1}>
        {/* curve bg svg component */}
        <CurveyBgDesign height="60%" />
        <View style={{ flexDirection: "row" }}>
          {/* welcome text */}
          <View style={styles.textView}>
            <Text style={styles.text}>Welcome {"\n"} Back</Text>
          </View>
          {/* Logo View */}
          <View style={styles.logoView}>
            <Image
              source={require("../assets/Logo.png")}
              resizeMode="contain"
              style={styles.logo}
            />
            <View style={styles.rightBottomrCircle}></View>
          </View>
          {/* Logo View */}
        </View>
      </View>
      <View style={styles.container2}>
        <KeyboardAwareScrollView style={styles.container}>
          <TextField
            placeholder="Your email address"
            type="email"
            label="Email"
          />
          <TextField
            placeholder="************"
            type="password"
            label="Password"
          />
          <View style={styles.buttonView}>
            <Button1 title="Continue" />
            <Button1 title="Signup" curve="left" onPress={()=>navigation.navigate('SignUp')} />
          </View>
          <TouchableOpacity style={styles.forgotText} onPress={()=>navigation.navigate('ForgotPassword')}>
            <Text style={styles.text2}>Forgot password?</Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  bgLayer: {
    backgroundColor: "#C4E2A6",
    position: "absolute",
    height: "100%",
    width: "100%",
    opacity: 0.8,
  },
  container1: {
    backgroundColor: "#B3D491",
    height: hp("51%"),
    width: wp("100%"),
  },
  textView: {
    top: "65%",
    marginHorizontal: wp("6%"),
    alignItems: "flex-start",
  },
  text: {
    fontWeight: "bold",
    color: "#fff",
    fontSize: rf(23),
    letterSpacing: 1,
    textAlign: "right",
  },
  logoView: {
    top: "60%",
    flex: 1,
    alignItems: "center",
    height: "80%",
  },
  logo: {
    width: "75%",
    height: "100%",
    right: wp("10%"),
  },
  rightBottomrCircle: {
    width: wp("8%"),
    height: wp("8%"),
    borderRadius: 100,
    backgroundColor: "#8FBC80",
    position: "absolute",
    opacity: 0.8,
    zIndex: 99999,
    right: 10,
    bottom: "55%",
    opacity: 0.5,
  },

  container2: {
    flex: 1,
    width: wp("85%"),
    marginHorizontal: wp("7.5%"),
    marginTop: hp("5%"),
  },
  buttonView: {
    alignItems: "center",
    paddingVertical: hp("2%"),
  },
  text2: {
    fontWeight: "500",
    color: "#fff",
    fontSize: rf(13),
    letterSpacing: 1,
    borderBottomWidth:0.8,
    borderColor:"#fff"
  },
  forgotText:{
    alignItems: "center",
  }
});
