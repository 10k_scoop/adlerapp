import React from "react";
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, TextInput } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';


export default function ChangePassword({ navigation }) {
    return (
        <View style={styles.body}>
            <View style={styles.Header}>
                <View style={styles.HeaderLayer}>
                    <ImageBackground
                        source={require('../assets/categoryIcons/profile2.jpg')}
                        style={{ width: wp('100%'), height: hp('35%') }}
                        resizeMode="cover"
                    >
                        <View style={styles.HeaderBar}>
                            <TouchableOpacity>
                                <AntDesign name="arrowleft" size={24} color="#fff" />
                            </TouchableOpacity>
                            <Text style={styles.Font1}>Setting</Text>
                            <TouchableOpacity>
                                <Entypo name="dots-three-horizontal" size={24} color="#fff" />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
            </View>
            <View style={styles.Discription}>
                <View style={styles.Name}>
                    <Text style={{ fontSize: rf(18) }}>Majid Ali Kaskheli</Text>
                    <Text style={styles.Font2}>Hyderabad ,pk</Text>
                </View>

                <View style={styles.profile}>
                    <Image
                        source={require('../assets/categoryIcons/profile2.jpg')}
                        style={{ width: "100%", height: "100%" }}
                        resizeMode="cover"
                    />
                </View>
                <View style={styles.Icon}>
                    <TouchableOpacity>
                        <MaterialIcons name="edit" size={rf(17)} color="#fff" />
                    </TouchableOpacity>
                </View>

                <View style={{ width: wp('90%'), height: hp('50%'), paddingHorizontal: 10 }}>
                    <View style={styles.PasswordChanges}>
                        <Text style={{ fontSize: rf(16), color: "grey" }}>Old Password</Text>
                        <TextInput
                            style={{ width: "100%", height: "40%", paddingHorizontal: 5 }}
                            secureTextEntry={true}
                            placeholder="***********"
                        />
                    </View>

                    <View style={styles.PasswordChanges}>
                        <Text style={{ fontSize: rf(16), color: "grey" }}>New Password</Text>
                        <TextInput
                            style={{ width: "100%", height: "40%", paddingHorizontal: 5 }}
                            secureTextEntry={true}
                        />
                    </View>

                    <View style={styles.PasswordChanges}>
                        <Text style={{ fontSize: rf(16), color: "grey" }}>Confirm Password</Text>
                        <TextInput
                            style={{ width: "100%", height: "40%", paddingHorizontal: 5 }}
                            secureTextEntry={true}
                        />
                    </View>
                </View>





            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Header: {
        width: wp('100%'),
        height: hp('30%'),
        backgroundColor: "#222"
    },
    HeaderBar: {
        width: "100%",
        height: "35%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10
    },
    Discription: {
        flex: 1,
        backgroundColor: "#fff",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingHorizontal: 15
    },
    profile: {
        width: hp('14%'),
        height: hp('14%'),
        borderWidth: 0.5,
        borderColor: "#e5e5e5",
        borderRadius: 100,
        position: "absolute",
        top: -45,
        left: 30,
        overflow: "hidden"
    },
    Icon: {
        width: hp('4%'),
        height: hp('4%'),
        borderRadius: 100,
        backgroundColor: "#222",
        position: "absolute",
        top: 20,
        right: "72%",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 2,
        borderColor: "#fff"
    },
    Name: {
        width: "100%",
        height: "18%",
        alignItems: "flex-end",
        paddingHorizontal: "18%",
        paddingVertical: 8
    },
    PasswordChanges: {
        width: "100%",
        height: "20%",
        borderBottomWidth: 1,
        borderColor: "#e5e5e5",
        paddingHorizontal: 5,
        justifyContent: "flex-end",
    },
    Switch: {
        width: wp('90%'),
        height: hp("5%"),
        alignItems: "center",
        flexDirection: "row",
        paddingHorizontal: 5,
    },
    HeaderLayer: {
        width: wp('100%'),
        height: hp('35%'),
        backgroundColor: "black",
        opacity: 0.8
    },


});
