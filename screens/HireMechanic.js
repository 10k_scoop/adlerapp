import React from "react";
import { StyleSheet, Text, View, ScrollView, TextInput } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import CategoryCard from "../components/CategoryCard"

export default function HireMechanic({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}
            <View style={styles.main}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.SearchBar}>
                        <TextInput
                            style={styles.SearchTextfield}
                            placeholder="Find a mechanic or any service"
                        />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory type='box' title="Plumber" bgcolor="#6672E0" halfCircleColor="#345CC1" img={require("../assets/categoryIcons/mechanic.png")} onpress={()=>navigation.navigate('BookingList')} />
                        <CategoryCard innerCategory type='box' title="Electrician" bgcolor="#D0F2FA" halfCircleColor="#38C8E8" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory type='box' title="AC Service" bgcolor="#A7DA97" halfCircleColor="#8FBC80" img={require("../assets/categoryIcons/mechanic.png")} />
                        <CategoryCard innerCategory type='box' title="Bike Repair" bgcolor="#D27575" halfCircleColor="#EBA2A2" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory  type='box' title="Car Repair" bgcolor="#D0F2FA" halfCircleColor="#38C8E8" img={require("../assets/categoryIcons/mechanic.png")} />
                        <CategoryCard innerCategory type='box' title="Doctor" bgcolor="#EFA0CB" halfCircleColor="#E066A8" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory type='box' title="Dhoobi" bgcolor="#D27575" halfCircleColor="#EBA2A2" img={require("../assets/categoryIcons/mechanic.png")} />
                        <CategoryCard innerCategory type='box' title="Home Appliance" bgcolor="#A7DA97" halfCircleColor="#8FBC80" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory type='box' title="Carpenter" bgcolor="#6672E0" halfCircleColor="#345CC1" img={require("../assets/categoryIcons/mechanic.png")} />
                        <CategoryCard innerCategory type='box' title="Mobile Repair" bgcolor="#D27575" halfCircleColor="#EBA2A2" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>
                    <View style={styles.categoryView}>
                        <CategoryCard innerCategory  type='box' title="Computer Repair" bgcolor="#D0F2FA" halfCircleColor="#38C8E8 " img={require("../assets/categoryIcons/mechanic.png")} />
                        <CategoryCard innerCategory type='box' title="Petrol" bgcolor="#EFA0CB" halfCircleColor="#E066A8" img={require("../assets/categoryIcons/mechanic.png")} />
                    </View>

                </ScrollView>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        alignItems: "center",
        marginTop: "7%",
    },
    SearchBar: {
        width: wp('90%'),
        height: hp('5%'),
        borderRadius: 100,
        borderWidth: 0.5,
        borderColor: "grey"
    },
    SearchTextfield: {
        width: "100%",
        height: "100%",
        paddingHorizontal: 10,
        fontSize: rf(16),
        color: "grey"
    },
    categoryView:{
        flex:1,
        marginVertical:15,
        flexDirection:'row',
        justifyContent:'space-between'
    }
});
