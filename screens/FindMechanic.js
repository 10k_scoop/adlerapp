import React from "react";
import { StyleSheet, Text, View, ScrollView, TextInput } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import { MaterialIcons } from '@expo/vector-icons';

export default function FindMechanic({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}
            <View style={styles.main}>
                <View style={styles.FindingMechanic}>
                    <View style={styles.SearchIcon}>
                    <MaterialIcons name="location-searching" size={50} color="black" />
                    </View>
                    <View style={styles.Font}>
                        <Text style={{fontSize:rf(14),fontWeight:"700"}}>Finding your mechanic wait... </Text>
                    </View>

                </View>

            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        backgroundColor: "#e5e5e5"
    },
    FindingMechanic: {
        width: wp('50%'),
        height: hp('18%'),
        backgroundColor: "#fff",
        borderRadius: 10,
        position: "absolute",
        top: "40%",
        right: "23%"
    },
    SearchIcon: {
        width: "100%",
        height: "60%",
       justifyContent:"center",
       alignItems:"center"
    },
    Font: {
        width: "100%",
        height: "40%",
        justifyContent:"center",
        alignItems:"center",
        
    }


});
