import React from 'react';
import { ImageBackground, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Entypo } from '@expo/vector-icons';
import Header from '../components/Header'
import Menu from '../components/Menu'
import BookingViewcard from '../components/BookingViewcard'
import { LinearGradient } from 'expo-linear-gradient';



export default function DashBoardAdmin() {
    return (

        <View style={styles.container}>
            <Header />
            <ScrollView>
                <View style={styles.HeadingTextContainer}>
                    <Text style={{ fontSize: rf(18) }}>Welcome Back, Majid</Text>
                </View>
                <View style={styles.MenuContainer}>
                    <View style={styles.MenuBody}>
                        <LinearGradient
                            // Background Linear Gradient
                            colors={['#8FBC80', '#B3D491']}
                            start={[0.0, 0.5]} end={[1.0, 0.5]} locations={[0.0, 1.0]}
                            style={{
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                top: 0,
                                height: "100%",
                                borderRadius: 15

                            }}
                        />
                        <View style={styles.MenuInnerBody}>
                            <TouchableOpacity style={styles.Mechenic}>
                                <Text style={{ fontSize: rf(16), color: '#8FBC80' }}>189</Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: rf(12), color: '#fff', marginTop: '5%' }}>Mechenic</Text>
                        </View>
                        <View style={styles.MenuInnerBody}>
                            <TouchableOpacity style={styles.Mechenic}>
                                <Text style={{ fontSize: rf(16), color: '#8FBC80' }}>189</Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: rf(12), color: '#fff', marginTop: '5%' }}>Bookings</Text>
                        </View>
                        <View style={styles.MenuInnerBody}>
                            <TouchableOpacity style={styles.Mechenic}>
                                <Text style={{ fontSize: rf(16), color: '#8FBC80' }}>189</Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: rf(12), color: '#fff', marginTop: '5%' }}>User</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.BookingsContainer}>
                    <View style={styles.BookingText}>
                        <Text style={{ fontSize: rf(18), color: '#222', }}>New bookings</Text>
                    </View>
                    <BookingViewcard name='nain' img={require('../assets/ProfilePhoto.png')} />
                    <BookingViewcard name='nain' img={require('../assets/ProfilePhoto.png')} />
                    <BookingViewcard name='nain' img={require('../assets/ProfilePhoto.png')} />
                    <BookingViewcard name='nain' img={require('../assets/ProfilePhoto.png')} />

                </View>
                <View style={styles.PicContainer}>
                    <Image style={{ height: "100%", width: '100%' }} source={require('../assets/PicMb.png')} />
                </View>
            </ScrollView>
            <Menu />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    HeadingTextContainer: {
        height: hp('7%'),
        width: wp('100%'),
        paddingHorizontal: wp('5%'),
        justifyContent: 'center'
    },
    MenuContainer: {
        height: hp('13%'),
        width: wp('100%'),
        paddingHorizontal: wp('5%'),
        justifyContent: 'center'

    },
    MenuBody: {
        flex: 1,
        backgroundColor: '#8FBC80',
        borderRadius: 15,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,

    },
    MenuInnerBody: {
        width: '28%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',

    },
    Mechenic: {
        height: hp('6%'),
        width: hp('6%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 100
    },
    BookingsContainer: {
        height: hp('46%'),
        width: wp('100%'),
        paddingHorizontal: wp('5%'),

    },
    BookingText: {
        height: '10%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    PicContainer: {
        height: hp('20%'),
        width: wp('100%'),
        padding: '5%',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('8%')
    }











});
