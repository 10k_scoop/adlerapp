import React from "react";
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';

export default function Settings({ navigation }) {
    return (
        <View style={styles.body}>
            <View style={styles.Header}>
                <View style={styles.HeaderLayer}>
                    <ImageBackground
                        source={require('../assets/categoryIcons/profile2.jpg')}
                        style={{ width: wp('100%'), height: hp('35%') }}
                        resizeMode="cover"
                    >
                        <View style={styles.HeaderBar}>
                            <TouchableOpacity>
                                <AntDesign name="arrowleft" size={24} color="#fff" />
                            </TouchableOpacity>
                            <Text style={styles.Font1}>Setting</Text>
                            <TouchableOpacity>
                                <Entypo name="dots-three-horizontal" size={24} color="#fff" />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
            </View>
            <View style={styles.Discription}>
                <View style={styles.Name}>
                    <Text style={{ fontSize: rf(18) }}>Majid Ali Kaskheli</Text>
                    <Text style={styles.Font2}>Hyderabad ,pk</Text>
                </View>

                <View style={styles.profile}>
                    <Image
                        source={require('../assets/categoryIcons/profile2.jpg')}
                        style={{ width: "100%", height: "100%" }}
                        resizeMode="cover"
                    />
                </View>
                <View style={styles.Icon}>
                    <TouchableOpacity>
                        <MaterialIcons name="edit" size={rf(17)} color="#fff" />
                    </TouchableOpacity>
                </View>

                <View style={styles.ProfileSettings}>
                    <Text style={styles.Font3}>Profile Settings</Text>
                </View>
                <View style={styles.Switch}>
                    <Text style={styles.Font2}>Push Notifications</Text>
                </View>
                <View style={styles.Switch}>
                    <Text style={styles.Font2}>Location</Text>
                </View>

                <View style={styles.Account}>
                    <Text style={styles.Font3}>Account</Text>
                </View>
                <View style={styles.AccountChanges}>
                    <Text style={styles.Font2}>Change Password</Text>
                    <TouchableOpacity onPress={()=>navigation.navigate('ChangePassword')}>
                        <AntDesign name="right" size={rf(16)} color="grey" />
                    </TouchableOpacity>
                </View>
                <View style={styles.AccountChanges}>
                    <Text style={styles.Font2}>Language</Text>
                    <Text style={styles.Font2}>english</Text>
                </View>
                <View style={styles.AccountChanges}>
                    <Text style={styles.Font2}>Verify Email</Text>
                    <TouchableOpacity>
                        <AntDesign name="right" size={rf(16)} color="grey" />
                    </TouchableOpacity>
                </View>

                <View style={styles.Support}>
                    <Text style={{ fontSize: rf(16), fontWeight: "600" }}>Support</Text>
                </View>
                <View style={styles.Help}>
                    <Text style={styles.Font2}>Instant Chat Support</Text>
                </View>
                <View style={styles.Help}>
                    <Text style={styles.Font2}>Feedback</Text>
                </View>


            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    Header: {
        width: wp('100%'),
        height: hp('30%'),
        backgroundColor: "#222"
    },
    HeaderBar: {
        width: "100%",
        height: "35%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10
    },
    Discription: {
        flex: 1,
        backgroundColor: "#fff",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingHorizontal: 15
    },
    profile: {
        width: hp('14%'),
        height: hp('14%'),
        borderWidth: 0.5,
        borderColor: "#e5e5e5",
        borderRadius: 100,
        position: "absolute",
        top: -45,
        left: 30,
        overflow: "hidden"
    },
    Icon: {
        width: hp('4%'),
        height: hp('4%'),
        borderRadius: 100,
        backgroundColor: "#222",
        position: "absolute",
        top: 20,
        right: "72%",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 2,
        borderColor: "#fff"
    },
    Name: {
        width: "100%",
        height: "18%",
        alignItems: "flex-end",
        paddingHorizontal: "18%",
        paddingVertical: 8
    },
    ProfileSettings: {
        width: wp('90%'),
        height: hp('4%'),
        borderBottomWidth: 1,
        borderColor: "#e5e5e5",
        paddingHorizontal: 5,
        marginBottom: 10
    },
    Switch: {
        width: wp('90%'),
        height: hp("5%"),
        alignItems: "center",
        flexDirection: "row",
        paddingHorizontal: 5,
    },
    Account: {
        width: wp('90%'),
        height: hp('4%'),
        borderBottomWidth: 1,
        borderColor: "#e5e5e5",
        paddingHorizontal: 5,
        marginBottom: 10,
        marginTop: 12
    },
    AccountChanges: {
        width: wp('90%'),
        height: hp("5%"),
        alignItems: "center",
        flexDirection: "row",
        paddingHorizontal: 5,
        justifyContent: "space-between"
    },
    Support: {
        width: wp('90%'),
        height: hp('4%'),
        borderBottomWidth: 1,
        borderColor: "#e5e5e5",
        paddingHorizontal: 5,
        marginTop: 10

    },
    Help: {
        width: wp('90%'),
        height: hp("5%"),
        justifyContent: "center",
        paddingHorizontal: 5,
    },
    HeaderLayer: {
        width: wp('100%'),
        height: hp('35%'),
        backgroundColor: "black",
        opacity: 0.8
    },
    Font1: {
        fontSize: rf(18),
        fontWeight: "700",
        color: "#fff"
    },
    Font2: {
        fontSize: rf(14),
        color: "grey"
    },
    Font3: {
        fontSize: rf(16),
        fontWeight: "600"
    }

});
