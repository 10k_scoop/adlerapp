import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import CategoryCard from "../components/CategoryCard";

export default function Dashboard({ navigation }) {
  return (
    <View style={styles.body}>
      {/* Header */}
      <Header1 title="Home" />
      {/* intro text and image */}
      <View style={styles.cont1}>
        {/* row1 Username and Tagline */}
        <View style={styles.row1}>
          <Text style={styles.usernameText}>Hi, Zeeshan Karim</Text>
          <Text style={styles.taglineText}>
            There are 6 restaurants delivering in your area.
            {"\n"}
            Get your favorite food now!
          </Text>
        </View>
        {/* row2Thumbnail */}
        <Image
          source={require("../assets/food/image1.png")}
          style={styles.image1}
          resizeMode="stretch"
        />
        {/* row2Thumbnail */}
      </View>
      <View style={{ width: "100%", height: "3%", alignItems: "center" }}>
        <Text style={{ color: "#EBA2A2", fontSize: rf(14) }}>Earn upto Rs 1000 credit limit </Text>
      </View>
      {/* Categories Cards Container */}
      <View style={styles.categoryCont}>
        <CategoryCard type='box' title="Hire mechanic & pay later" bgcolor="#A7DA97" halfCircleColor="#8FBC80" img={require("../assets/categoryIcons/mechanic.png")} onpress={()=>navigation.navigate('HireMechanic')} />
        <CategoryCard reverse type='box' tagline="Now order food you love and pay later !" title="Food Delivery" bgcolor="#B8568B" halfCircleColor="#EFA0CB" img={require("../assets/food/image2.png")} />
      </View>
      <View style={styles.categoryCont}>
        <CategoryCard type='row' height={hp('15%')} width={wp('90%')} dots="first" title1="Grocery & Vegetables" title2="Order anything you want ,pay us later" icon="" bgcolor="#F9C1D5" halfCircleColor="#E066A8" img={require('../assets/categoryIcons/grocery.png')} />
      </View>
      <View style={styles.categoryCont}>
        <CategoryCard type='row' height={hp('15%')} width={wp('90%')} dots="second" title1="Order medicines we care for your health" title2="Order anything you want ,pay us later" icon="" bgcolor="#D0F2FA" halfCircleColor="#38C8E8" img={require('../assets/categoryIcons/medicine.png')} />
      </View>
      {/* Categories Cards Container */}
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor:'#fff'
  },
  cont1: {
    marginHorizontal: wp("4%"),
    flexDirection: "row",
    marginTop: 10,
    height: hp("15%"),
    justifyContent: "center",
  },
  usernameText: {
    fontSize: rf(15),
    color: "#222",
    fontWeight: "bold",
  },
  row1: {
    flex: 1,
    justifyContent: "center",
  },
  image1: {
    right: 0,
    position: "absolute",
    width: "48%",
    height: "100%",
  },
  taglineText: {
    marginTop: 5,
    fontSize: rf(12),
  },
  categoryCont: {
    marginHorizontal: wp("4%"),
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom:hp('2%'),
    marginTop:10
  },
});
