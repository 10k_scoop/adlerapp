import React from "react";
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import Header1 from "../components/Header1";
import Sender from "../components/Sender";
import Reciever from "../components/Reciever";
import { FontAwesome } from '@expo/vector-icons';

export default function InstantChat({ navigation }) {
    return (
        <View style={styles.body}>
            {/* Header */}
            <Header1 title="Home" />
            {/* intro text and image */}
            
            <View style={styles.main}>
                <View style={{ marginTop: "10%" }}>
                    <Sender msg="kithy ahen ach bhala" />
                    <Reciever msg="ha achan thO" />
                    <Sender msg="kithy ahen ach bhala" />
                    <Reciever msg="ha achan thO" />
                    <Sender msg="kithy ahen ach bhala" />
                    <Reciever msg="ha achan thO" />
                    <Sender msg="kithy ahen ach bhala" />
                    
                </View>
            </View>
            

            <View style={styles.Keyboard}>
                <TextInput
                    style={styles.TextField}
                    placeholder="type something......."
                />
                <TouchableOpacity>
                    <FontAwesome name="send" size={22} color="#8FBC80" />
                </TouchableOpacity>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#fff',
    },
    main: {
        flex: 1,
        backgroundColor: "#e5e5e5",
        alignItems: "center",
    },
    Keyboard: {
        width: wp('100%'),
        height: hp('10%'),
        backgroundColor: "#fff",
        position: "absolute",
        bottom: 0,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        paddingHorizontal: 5
    },
    TextField: {
        width: "80%",
        height: "60%",
        borderWidth: 0.2,
        borderRadius: 100,
        backgroundColor:"#e5e5e5",
        paddingHorizontal:8
        
    }


});
