import React from 'react';
import { ImageBackground, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Header1 from '../components/Header1'
import { Entypo } from '@expo/vector-icons';
import Button1 from '../components/Button1'

export default function RatingForMechenicRider() {
    return (

        <View style={styles.container}>
            <Header1 title='Home' />
            <View style={styles.MapView}>

            </View>
            <View style={styles.ReviewContainer}>
                <View style={styles.ReviewTextContainer}>
                    <Text style={{ fontSize: rf(18) }}>Your reviews matter!</Text>
                </View>
                <View style={styles.ReviewTextContainer}>
                    <Text style={{ fontSize: rf(14) }}>Machenic</Text>
                </View>

                <View style={styles.StarContainer}>
                    <View style={styles.StarBody}>
                        <Entypo name="star" size={28} color="#FFE600" />
                        <Entypo name="star" size={28} color="#FFE600" />
                        <Entypo name="star" size={28} color="#FFE600" />
                        <Entypo name="star" size={28} color="#222" />
                        <Entypo name="star" size={28} color="#222" />
                    </View>
                </View>
            </View>
            <View style={styles.CommentsContainer}>
                <View style={styles.Comments}>
                    <Text style={{ fontSize: rf(16) }}>Thankyou for choosing us !</Text>
                </View>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',


    },
    MapView: {
        height: hp('100%'),
        width: wp('100%'),
        backgroundColor: '#e5e5e5'
    },
    ReviewContainer: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    ReviewTextContainer: {
        height: '18%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    StarContainer: {
        height: '15%',
        alignItems: 'center'
    },
    StarBody: {
        height: '100%',
        width: '45%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between"
    },
    CommentsContainer: {
        height: hp('6%'),
        width: wp('100%'),
        position: 'absolute',
        bottom: hp('35%'),
        paddingHorizontal: wp('5%')
    },
    Comments: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,

        elevation: 15,
    }



});
