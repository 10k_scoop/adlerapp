import React from "react";
import { TextInput, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
export default function TextField({ placeholder, type, showPassword,label }) {
  return (
    <View style={styles.body}>
      <Text style={styles.fieldText}>{label}</Text>
      <View style={styles.fieldView}>
        <TextInput
          secureTextEntry={type == "password" ? true : false}
          style={styles.textField}
          placeholder={placeholder}
          placeholderTextColor="#fff"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 0,
    marginVertical:hp('1.5%')
  },
  textField: {
    flex: 1,
    color: "#fff",
  },
  fieldText: {
    color: "#fff",
    fontSize: rf(16),
    fontWeight: "500",
    letterSpacing: 0.5,
  },
  fieldView: {
    borderBottomWidth: 1,
    borderColor: "#fff",
    width: "100%",
    height: hp("4.5%"),
  },
});
