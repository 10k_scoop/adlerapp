import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function Reciever({ navigation, msg,}) {

    return (
        <View style={style.main}>
            <TouchableOpacity style={style.Message}>
                <Text style={{fontSize:rf(14),color:"#fff"}}>{msg}</Text>

            </TouchableOpacity>
        </View>
    )
}

const style = StyleSheet.create({

    Message: {
        maxWidth: '90%',
        backgroundColor: "grey",
        padding: 10,
        marginBottom: 10,
        borderRadius: 20
    },
    main: {
        width: wp('90%'),
        marginBottom: "2%",
        alignItems: "flex-end"
    },
  



})