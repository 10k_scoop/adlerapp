import React from 'react';
import { ImageBackground, StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Entypo } from '@expo/vector-icons';


export default function Header() {
    return (

        <View style={styles.container}>
            <TouchableOpacity style={styles.IconBody}>
                <Image style={{ height: '100%', width: '100%' }} source={require('../assets/Male.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.ProfileBody}>
                <Image style={{ height: '100%', width: '100%' }} source={require('../assets/ProfilePhoto.png')} />
            </TouchableOpacity>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: hp('12%'),
        width: wp('100%'),
        backgroundColor: '#fff',
        paddingHorizontal: wp('5%'),
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingVertical:hp('2%')
    },
    IconBody: {
        height: hp('4%'),
        width: hp('4%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
    ProfileBody: {
        height: hp('6%'),
        width: hp('6%'),
        borderRadius: 100
    }






});
