import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function BookingViewCard({ navigation,name,num,img,onPress }) {

    return (
        <TouchableOpacity style={styles.Message} onPress={onPress}>
            <View style={styles.Profile}>
                <Image
                    source={img}
                    resizeMode="cover"
                    style={{ width: "100%", height: "100%" }}
                />
            </View>
            <View style={styles.Name}>
                <Text style={{ fontSize: rf(16), color: "#fff" }}>{name}</Text>
                <Text style={{ fontSize: rf(12), color: "grey" }}>{num}</Text>
            </View>
            <View style={styles.BookingView}>
                <Text style={{ fontSize: rf(14), color: "#fff" }}>View Booking</Text>
            </View>

        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({

    Message: {
        width: wp('90%'),
        height: hp('8%'),
        backgroundColor: "#8FBC80",
        borderRadius: 20,
        marginBottom:"6%",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    Profile: {
        width: hp('6%'),
        height: hp('6%'),
        borderRadius: 100,
        borderWidth: 0.2,
        borderColor: "grey",
        overflow: "hidden"
    },
    Name: {
        width: "47%",
        height: "50%",
        paddingHorizontal: 8
    },
    BookingView: {
        width: "40%",
        height: "50%",
        justifyContent: "center",
        alignItems: "center",
    }

})