import React from "react";
import {StyleSheet, View, } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Svg, { Path, Rect, Circle } from "react-native-svg";
export default function CurveyBgDesign({height,rightMiddleButtonPosition}) {
  return (
    <View style={styles.cont}>
      {/* corner circles */}
      <View style={styles.leftCornerCircle}></View>
      <View style={styles.rightCornerCircle}></View>
      <View style={[styles.rightmiddleCircle,{bottom:rightMiddleButtonPosition || hp('23%')}]}></View>
      {/* corner circles */}
      {/* Right side rectangle svg */}
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 700 320"
        style={{
          position: "absolute",
          left: wp("68%"),
          bottom: hp("17%"),
        }}
      >
        <Rect width={wp("70%")} height="250" strokeWidth="2" fill="#8FBC80" />
      </Svg>
      {/* Right side rectangle svg */}

      {/* Right side curve svg */}
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 700 320"
        style={{
          transform: [{ rotate: "38deg" }],
          position: "absolute",
          bottom: hp("10%"),
          left: wp("22%"),
        }}
      >
        <Path
          fill="#8FBC80"
          fill-opacity="1"
          d="M0,0L24,48C48,96,96,192,144,240C192,288,240,288,288,261.3C336,235,384,181,432,181.3C480,181,528,235,576,266.7C624,299,672,309,720,272C768,235,816,149,864,128C912,107,960,149,1008,149.3C1056,149,1104,107,1152,128C1200,149,1248,235,1296,266.7C1344,299,1392,277,1416,266.7L1440,256L1440,0L1416,0C1392,0,1344,0,1296,0C1248,0,1200,0,1152,0C1104,0,1056,0,1008,0C960,0,912,0,864,0C816,0,768,0,720,0C672,0,624,0,576,0C528,0,480,0,432,0C384,0,336,0,288,0C240,0,192,0,144,0C96,0,48,0,24,0L0,0Z"
        ></Path>
      </Svg>
      {/* Right side curve svg */}

      {/* Bottom Curve SVG */}
      <Svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1300 320"
        style={{ position: "absolute", top: height }}
      >
        <Path
          fill="#B3D491"
          fill-opacity="1"
          d="M0,96L48,80C96,64,192,32,288,32C384,32,480,64,576,96C672,128,768,160,864,170.7C960,181,1056,171,1152,144C1248,117,1344,75,1392,53.3L1440,32L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
        ></Path>
      </Svg>
      {/* Bottom Curve SVG */}
    </View>
  );
}

const styles = StyleSheet.create({
  cont: {
    position: "absolute",
    height:'100%',
    width:'100%'
    
  },
  leftCornerCircle: {
    width: wp("17%"),
    height: wp("17%"),
    borderRadius: 100,
    backgroundColor: "#8FBC80",
    position: "absolute",
    left: "-2%",
    top: "-2%",
    opacity: 0.7,
    zIndex: 99999,
  },
  rightCornerCircle: {
    width: wp("15%"),
    height: wp("15%"),
    borderRadius: 100,
    backgroundColor: "#B3D491",
    position: "absolute",
    right: "-2%",
    top: "-2%",
    opacity: 0.8,
    zIndex: 99999,
  },
  rightmiddleCircle: {
    width: wp("12%"),
    height: wp("12%"),
    borderRadius: 100,
    backgroundColor: "#8FBC80",
    position: "absolute",
    right: wp("22%"),
    opacity: 0.8,
    zIndex: 99999,
    opacity: 0.6,
  },

});
