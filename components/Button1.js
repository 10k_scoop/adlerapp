// Button1 component usage and props
//Props (properties):-
// width , height for button body... ( default width is wp('50%') and height is hp('7%') )
// fontSize , title for button text ( default fontSize is rf(15) and default title is 'title goes here' )
//   event for touchableOpacity ( default none )
//top change curve deisgn use prop curve=left or curve=right
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
export default function Button1({
  width,
  height,
  title,
  fontSize,
  onPress,
  curve,
}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.btnView,
        { transform: [{ rotateY: curve == "left" ? "180deg" : "0deg" }] },
      ]}
    >
      <View style={styles.btn}>
        <Text
          style={[
            styles.buttonText,
            { transform: [{ rotateY: curve == "left" ? "180deg" : "0deg" }] },
          ]}
        >
          {title}
        </Text>
        <View style={styles.cir}></View>
        <View style={styles.cir2}></View>
        <View style={styles.cir3}></View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnView: {
    marginVertical: hp("1%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    borderRadius: 100,
  },
  btn: {
    width: wp("60%"),
    height: hp("5%"),
    backgroundColor: "#B3D491",
    borderRadius: 100,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
  },
  cir: {
    width: wp("13%"),
    height: wp("10%"),
    backgroundColor: "#8FBC80",
    position: "absolute",
    right: wp("1%"),
    borderRadius: 100,
    transform: [{ scaleX: 1.8 }],
    bottom: 15,
  },
  cir2: {
    width: wp("10%"),
    height: wp("10%"),
    backgroundColor: "#8FBC80",
    position: "absolute",
    right: -10,
    borderRadius: 100,
    bottom: -15,
  },
  cir3: {
    width: wp("5%"),
    height: wp("5%"),
    backgroundColor: "#B3D491",
    position: "absolute",
    right: 10,
    borderRadius: 100,
    bottom: -4,
    right: 15,
    transform: [{ scaleX: 1.2 }],
  },
  buttonText: {
    fontSize: rf(15),
    color: "#fff",
    fontWeight: "700",
  },
});
