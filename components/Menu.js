import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from 'expo-linear-gradient';
export default function Menu({ navigation, }) {

    return (
        <View style={styles.MainContainer}>
            <LinearGradient
                // Background Linear Gradient
                colors={['#8FBC80', '#B3D491']}
                start={[0.0, 0.5]} end={[1.0, 0.5]} locations={[0.0, 1.0]}
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    height: "100%",
                    borderRadius: 15

                }}
            />
            <TouchableOpacity style={styles.Booking}>
                <Image style={{ height: '70%', width: '70%' }} source={require('../assets/Booking.png')} resizeMode='center' />
                <Text style={{ fontSize: rf('12') }}>Booking</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Booking}>
                <Image style={{ height: '70%', width: '70%' }} source={require('../assets/Booking.png')} resizeMode='center' />
                <Text style={{ fontSize: rf('12') }}>Mechenics</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Booking}>
                <Image style={{ height: '70%', width: '100%' }} source={require('../assets/ProfilePhoto.png')} resizeMode='center' />
                <Text style={{ fontSize: rf('12') }}>Profile</Text>
            </TouchableOpacity>
        </View>

    )
}

const styles = StyleSheet.create({

    MainContainer: {
        height: hp('8%'),
        width: wp('100%'),
        backgroundColor: '#8FBC80',
        position: 'absolute',
        flexDirection: 'row',
        paddingHorizontal: wp('8%'),
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'space-between'

    },
    Booking: {
        height: '60%',
        width: '15%',
        alignItems: 'center',
        justifyContent: 'space-between'
    },



})