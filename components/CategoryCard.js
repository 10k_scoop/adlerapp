import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
export default function CategoryCard({
  bgcolor,
  icon,
  title,
  halfCircleColor,
  img,
  width,
  height,
  type,
  tagline,
  reverse,
  title1,
  dots,
  onpress,
  innerCategory
}) {
  return (
    <TouchableOpacity style={[styles.body, { backgroundColor: bgcolor, width: width || wp("40%"), height: height || hp("20%"), }]} onPress={onpress}>
      {/* First Category Type */}
      {type == 'box' ?
        <View style={[styles.firstView, { flexDirection: reverse ? 'column-reverse' : 'column', bottom: reverse ? 10 : 0 }]}>
          <View
            style={[styles.halfCircle1, { backgroundColor: halfCircleColor }]}
          ></View>
          <View
            style={[styles.halfCircle2, { backgroundColor: halfCircleColor }]}
          ></View>
          <View style={[styles.firstImageView, { alignItems: 'flex-end' }]}>
            <Image
              source={img}
              resizeMode="contain"
              style={[styles.imageIcon, { width: reverse ? '75%' : '100%', height: reverse ? '90%' : '100%' }]}
            />

          </View>
          <View style={[styles.textView,{alignItems:innerCategory?'center':'flex-start'}]}>
            <Text style={[styles.text]}>{title}</Text>
            <Text style={[styles.headingTaglineText, { color: '#fff', display: tagline ? 'flex' : 'none' }]}>{tagline}</Text>
          </View>
        </View>
        : type == 'row' ?
          < View style={styles.secondCard}>
            {/* Circles */}
            <View
              style={[styles.SecondhalfCircle2, { backgroundColor: halfCircleColor }]}
            ></View>
            {/* Circles */}

            <View style={styles.secondTitleView}>
              <Text style={[styles.headingText]}>{title1}</Text>
              <Text style={[styles.headingTaglineText]}>Order anything you want ,
                pay us later</Text>
            </View>
            <View style={styles.secondImageView}>
              <Image source={img} resizeMode="cover" style={{ width: '95%', height: '90%' }} />

              {dots == "first" ?
                < View style={{ position: "absolute", right: 2 }}>
                  <View style={styles.FirstCardLargeDot}>
                  </View>
                  <View style={styles.FirstCardMediumDot}>
                  </View>
                  <View style={styles.FirstCardSmallDot}>
                  </View>
                </View>
                : dots == "second" ?
                  <View style={{ position: "absolute", right: 2 }}>
                    <View style={styles.secondCardLargeDot}>
                    </View>
                    <View style={styles.secondCardMediumDot}>
                    </View>
                    <View style={styles.secondCardSmallDot}>
                    </View>
                  </View>
                  : undefined
              }
            </View>
          </View>
          : undefined
      }
      {/* Second Category Type */}




    </TouchableOpacity >
  );
}

const styles = StyleSheet.create({
  body: {
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  halfCircle1: {
    width: 50,
    height: 50,
    backgroundColor: "black",
    borderRadius: 100,
    position: "absolute",
    left: -25,
    top: hp("7%"),
  },
  halfCircle2: {
    width: 50,
    height: 50,
    backgroundColor: "black",
    borderRadius: 100,
    position: "absolute",
    right: -25,
    top: 10,
  },
  textView: {
    
    justifyContent: "center",
    flex: 0.3,
    marginBottom: 10,
    paddingHorizontal: 7
  },
  imageIcon: {
    width: '100%',
    height: '100%',
  },
  text: {
    fontSize: rf(15),
    color: "#fff",
    fontWeight: '600',
  },
  secondCard: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    paddingHorizontal: 10
  },
  SecondhalfCircle2: {
    width: 50,
    height: 50,
    backgroundColor: "black",
    borderRadius: 100,
    position: "absolute",
    left: 25,
    bottom: -25,
  },
  secondTitleView: {
    flex: 0.6,
    marginTop: '5%'
  },
  secondImageView: {
    flex: 0.4,
  },
  headingText: {
    color: '#222',
    fontSize: rf(15),
    fontWeight: '700',
    marginBottom: 5
  },
  headingTaglineText: {
    color: '#222',
    fontSize: rf(12),
    fontWeight: '500',
  },
  firstImageView: {
    flex: 0.7
  },
  firstView: {
    width: '100%',
    height: '100%',
  },
  FirstCardLargeDot: {
    width: 8,
    height: 8,
    borderRadius: 100,
    right: -5,
    top: 3,
    position: "absolute",
    backgroundColor: "#E066A8"
  },
  FirstCardMediumDot: {
    width: 5,
    height: 5,
    borderRadius: 100,
    right: 4,
    top: 10,
    position: "absolute",
    backgroundColor: "#E066A8"
  },
  FirstCardSmallDot: {
    width: 4,
    height: 4,
    borderRadius: 100,
    right: 10,
    top: 15,
    position: "absolute",
    backgroundColor: "#E066A8"
  },
  secondCardLargeDot: {
    width: 8,
    height: 8,
    borderRadius: 100,
    right: -5,
    top: 3,
    position: "absolute",
    backgroundColor: "#38C8E8"
  },
  secondCardMediumDot: {
    width: 5,
    height: 5,
    borderRadius: 100,
    right: 4,
    top: 10,
    position: "absolute",
    backgroundColor: "#38C8E8"
  },
  secondCardSmallDot: {
    width: 4,
    height: 4,
    borderRadius: 100,
    right: 10,
    top: 15,
    position: "absolute",
    backgroundColor: "#38C8E8"
  }
});
