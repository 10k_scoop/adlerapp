import React from "react";
import { StyleSheet, Text, View,TouchableOpacity,StatusBar,Platform } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFPercentage, RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { FontAwesome,FontAwesome5 } from "@expo/vector-icons";
export default function Header1({ title }) {
  return (
    <View style={styles.body}>
      <LinearGradient
        // Background Linear Gradient
        colors={["#B3D491", "#8FBC80"]}
        start={[0.0, 0.5]}
        end={[1.0, 0.5]}
        style={styles.background}
      />
      <View style={styles.cont1}>
        <TouchableOpacity style={styles.icon}>
          <FontAwesome name="user-circle" size={rf(22)} color="#fff" />
        </TouchableOpacity>
        <View style={styles.title}>
          <Text style={styles.text}>{title}</Text>
        </View>
        <TouchableOpacity style={styles.icon2}>
          <FontAwesome5 name="power-off" size={rf(18)} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 0,
    height: hp("11%"),
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    flex: 1,
    height: "100%",
    width: "100%",
  },
  cont1: {
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
    alignItems: "center",
    flex: 1,
    marginTop:Platform.OS=='ios'?StatusBar.currentHeight+hp('5%'):StatusBar.currentHeight+hp('2%')
  },
  icon: {
    flex: 0,
    marginRight: wp("5%")
  },
  title:{
    flex:1
  },
  text:{
    color:'#fff',
    fontSize:rf(17),
    fontWeight:'700'
  },
  icon2:{
    flex: 0,
  }
});
