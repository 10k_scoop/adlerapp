import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from 'expo-linear-gradient';
export default function BookingCard({ navigation,Service,Area,Phone,Address,Status,img,Name,Ref, onPress }) {

    return (
        <TouchableOpacity style={styles.Card} onPress={onPress}>
            <LinearGradient
                // Background Linear Gradient
                colors={['#8FBC80', '#B3D491']}
                start={[0.0, 0.5]} end={[1.0, 0.5]} locations={[0.0, 1.0]}
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    height: "100%",
                    borderRadius: 15

                }}
            />
            <View style={styles.FirstRow}>
                <View style={styles.profile}>
                    <Image
                        source={img}
                        style={{ width: "100%", height: "100%" }}
                    />
                </View>
                <View style={styles.name}>
                    <Text style={{ fontSize: rf(18), color: "#fff" }}>{Name}</Text>
                </View>
                <View style={styles.Reference}>
                    <Text style={{ fontSize: rf(16), color: "#fff" }}>{Ref}</Text>
                </View>
            </View>
            <View style={styles.SecondRow}>
                <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700" }}>Service : <Text style={{fontSize:rf(16),fontWeight:"600"}}>{Service}</Text></Text>
                <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700" }}>Area : <Text style={{fontSize:rf(16),fontWeight:"600"}}>{Area}</Text></Text>
                <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700" }}>Contact : <Text style={{fontSize:rf(16),fontWeight:"600"}}>{Phone}</Text></Text>
                <Text style={{ fontSize: rf(18), color: "#fff",fontWeight:"700"  }}>Address :{'\n'}<Text style={{fontSize:rf(16),fontWeight:"600"}}>{Address}</Text></Text>
            </View>
            <View style={styles.ThirdRow}>
                <Text style={{ fontSize: rf(26), color: "#fff", fontWeight: "700" }}>Status</Text>
                <View style={styles.status}>
                    <Text style={{ fontSize: rf(18), color: "#fff", fontWeight: "500" }}>{Status}</Text>
                </View>
                <Text style={{ fontSize: rf(24), color: "#fff", fontWeight: "700" }}>Location</Text>
               
            </View>


        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({

    Card: {
        width: wp('90%'),
        height: hp('80%'),
        backgroundColor: "#8FBC80",
        borderRadius: 20,
        paddingHorizontal: 10,
    },
    Profile: {
        width: hp('6%'),
        height: hp('6%'),
        borderRadius: 100,
        borderWidth: 0.2,
        borderColor: "grey",
        overflow: "hidden"
    },
    Name: {
        width: "47%",
        height: "50%",
        paddingHorizontal: 8
    },
    BookingView: {
        width: "40%",
        height: "50%",
        justifyContent: "center",
        alignItems: "center",
    },
    FirstRow: {
        width: "100%",
        height: "12%",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,

    },
    profile: {
        width: hp('6%'),
        height: hp('6%'),
        borderRadius: 100,
        overflow: "hidden"
    },
    name: {
        width: "45%",
        height: "50%",
        paddingHorizontal: 10,
        justifyContent: "center"
    },
    Reference: {
        width: "40%",
        height: "50%",
        paddingHorizontal: 5,
        alignItems: "flex-end",
        justifyContent: "center"
    },
    SecondRow: {
        width: "100%",
        height: "22%",
        justifyContent: "center",
        paddingHorizontal: 10,

    },
    ThirdRow: {
        width: "100%",
        height: "20%",
        alignItems: "center",
        justifyContent:"space-evenly",
    },

})